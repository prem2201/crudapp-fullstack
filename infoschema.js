const mongoose = require("mongoose");

const infoSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  salary: {
    type: Number,
    required: true,
  },
  age: {
    type: Number,
    required: true,
  },
  createTime: {
    type: Date,
    default: Date.now,
  },
});
module.exports = mongoose.model("Info", infoSchema);
