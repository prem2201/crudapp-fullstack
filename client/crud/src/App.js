import Grid from "@material-ui/core/Grid";
import axios from "axios";
import React, { useEffect, useState } from "react";
import Form from "./form";
import Table from "./table";

function App() {
  const [data, setData] = useState([]);
  const [editdata, setEditdata] = useState([]);
  const create = (dataa) => {
    if (!dataa.isEdit) {
      console.log("create from api");
      console.log(data);
      axios
        .post("http://localhost:5000/info", {
          name: dataa.name,
          salary: dataa.salary,
          age: dataa.age,
        })
        .then((res) => {
          getAll();
        });
    } else {
      console.log("update from api");
      console.log(dataa._id);
      axios
        .put(`http://localhost:5000/info/update`, {
          id: dataa.id,
          name: dataa.name,
          salary: dataa.salary,
          age: dataa.age,
        })
        .then((res) => {
          window.location.reload();
          getAll();
        });
    }
  };
  useEffect(() => {
    getAll();
  });
  const getAll = () => {
    axios.get("http://localhost:5000/info").then((res) => {
      setData(res.data);
    });
  };

  const update = (data) => {
    console.log(data);
    setEditdata(data);
  };
  const del = (dataa) => {
    console.log(dataa._id);
    var option = window.confirm(`are you want to delete ${dataa.name}`);
    if (option) {
      axios
        .delete(`http://localhost:5000/info/del`, {
          _id: dataa.id,
          name: dataa.name,
          age: dataa.age,
          salary: dataa.salary,
        })
        .then((res) => {
          console.log(res);
          getAll();
        });
    }
  };
  return (
    <div style={{ padding: 20, backgroundColor: "#263238" }} className="App">
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <center>
            <h1 style={{ color: "white" }}>CRUD APPLICATION</h1>
          </center>
        </Grid>
        <Grid item xs={12} md={4}>
          <div style={{ padding: 16, backgroundColor: "white" }}>
            <Form myData={create} setForm={editdata} />
          </div>
        </Grid>
        <Grid item xs={12} md={8}>
          <div style={{ padding: 20, backgroundColor: "white" }}>
            <Table getData={data} setData={update} del={del} />
          </div>
        </Grid>
      </Grid>
    </div>
  );
}

export default App;
