import { IconButton } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Typography from "@material-ui/core/Typography";
import React from "react";
import { FiEdit } from "react-icons/fi";
import { MdDeleteForever } from "react-icons/md";

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 650,
  },
  img: {
    borderRadius: "50%",
    height: 80,
    width: 80,
  },
  float: {
    float: "right",
    fontSize: 26,
    color: "red",
    [theme.breakpoints.down("sm")]: {
      fontSize: 20,
    },
    "&:hover": {
      color: "#5550D3",
    },
  },
  floatl: {
    float: "left",
    fontSize: 26,
    color: "green",
    [theme.breakpoints.down("sm")]: {
      fontSize: 20,
    },
    "&:hover": {
      color: "#5550D3",
    },
  },
}));

export default function Tables(props) {
  const classes = useStyles();
  return (
    <>
      <Grid container>
        <Grid item xs={12}>
          <br />

          <Typography variant="h4" color="primary">
            User Details
          </Typography>

          <br />
        </Grid>
        <br />
        <TableContainer component={Paper}>
          <Table className={classes.table} size="small">
            <TableHead>
              <TableRow>
                <StyledTableCell>Name</StyledTableCell>
                <StyledTableCell>Age</StyledTableCell>
                <StyledTableCell>Salary</StyledTableCell>
                <StyledTableCell>Edit</StyledTableCell>
                <StyledTableCell>Delete</StyledTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {props.getData.map((e, index) => (
                <StyledTableRow key={e.id}>
                  <StyledTableCell>{e.name}</StyledTableCell>
                  <StyledTableCell>{e.age}</StyledTableCell>
                  <StyledTableCell>{e.salary}</StyledTableCell>
                  <StyledTableCell>
                    <IconButton
                      color="primary"
                      size="small"
                      onClick={(event) => {
                        props.setData(e);
                      }}
                    >
                      <FiEdit />
                    </IconButton>
                  </StyledTableCell>
                  <StyledTableCell>
                    <IconButton
                      color="secondary"
                      onClick={(event) => {
                        props.del(e);
                      }}
                    >
                      <MdDeleteForever />
                    </IconButton>
                  </StyledTableCell>
                </StyledTableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    </>
  );
}
