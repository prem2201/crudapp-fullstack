const express = require("express");
const router = express.Router();
const InfoRouter = require("./infoschema");
//create
router.post("/", async (req, res) => {
  var data = new InfoRouter({
    name: req.body.name,
    age: req.body.age,
    salary: req.body.salary,
  });
  await data.save();
  res.json(data);
});
//getAll
router.get("/", async (req, res) => {
  var findData = await InfoRouter.find();
  res.json(findData);
});
//update
router.put("/update", async (req, res) => {
  console.log(req);
  var update = await InfoRouter.updateOne(
    { id: req.params.id },
    {
      $set: {
        name: req.body.name,
        age: req.body.age,
        salary: req.body.salary,
      },
    }
  );
  res.json(update);
});
//deleter
router.delete("/del", async (req, res) => {
  var delData = await InfoRouter.findOneAndRemove(req.params.id).then((e) => {
    res.json({ message: "deleted successfully" });
  });

  res.json(delData);
});
router.get("/", (req, res) => {
  res.json("i am from router");
});
module.exports = router;
