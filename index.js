//third party module
const express = require("express");
const app = express();
const mongoose = require("mongoose");
const morgan = require("morgan");
const cors = require("cors");
//cheking
// app.use("/", (req, res) => {
//   res.json("Hi friends");
// });
//middleware
app.use(cors());
app.use(morgan("dev"));
app.use(express.json());
//Router
const infoRouter = require("./router");
app.use("/info", infoRouter);

//listen port
app.listen(5000, () => {
  console.log("server start on 5000");
});

//database connection
mongoose.connect(
  "mongodb://localhost/crudapp",
  { useNewUrlParser: true, useUnifiedTopology: true },
  (err) => {
    if (!err) {
      console.log("DB connected successfully");
    }
  }
);
